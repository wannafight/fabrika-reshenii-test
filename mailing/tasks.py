import logging

from django.conf import settings
from django.utils import timezone

from mailing.models import Mailing
from fabriqueStudioTest.celery import app
from mailing.services import send_message_and_update_status

logger = logging.getLogger('celery')


@app.task
def send_mailing_messages(mailing_id: int):
    mailing_obj = Mailing.objects.filter(pk=mailing_id).first()

    if not mailing_obj:
        logger.warning(f"Mailing({mailing_id}) does not exist")
        return

    now = timezone.now()

    if mailing_obj.end_dt < now:
        logger.warning(
            f"{mailing_obj} end time has met: {mailing_obj.end_dt}"
        )
        return

    delta = (
        now
        + timezone.timedelta(seconds=settings.MAILING_TIME_THRESHOLD_SECONDS)
    )
    if mailing_obj.start_dt > delta:
        logger.info(
            f"{mailing_obj} start_dt significantly "
            f"greater than now time ({now})"
        )
        return

    logger.info(f"Started doing mailing sends for {mailing_obj}")
    mailing_text = mailing_obj.text
    for message in mailing_obj.messages.all():
        print('')
        send_message_and_update_status(
            message.client_id, message.pk, mailing_text
        )
