from typing import Any


def get_value_from_dict_or_instance(key: str,
                                    data_dict: dict,
                                    instance: object | None) -> Any:
    value = data_dict.get(key)
    if instance and not value:
        return getattr(instance, key)
    return value
