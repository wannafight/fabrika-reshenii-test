from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from mailing.models import Client, Mailing
from mailing.serializers import ClientSerializer, MailingSerializer, \
    MailingStatusSerializer


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.prefetch_related('messages')

    @action(detail=True, methods=['get'], url_path='stats',
            serializer_class=MailingStatusSerializer)
    def get_mailing_stats(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)
