import random

from django.core.management import BaseCommand

from mailing.models import Client


class Command(BaseCommand):
    def handle(self, *args, **options):
        to_create = []
        clients_to_create = 20

        for i in range(clients_to_create//2):
            phone_number = f"7904{random.randint(1000000, 9999999)}"
            tag = random.choice(['tag1', 'tag2'])
            to_create.append(
                Client(phone_number=phone_number,
                       tag=tag, carrier_code='904')
            )

        for i in range(clients_to_create - clients_to_create//2):
            phone_number = f"7994{random.randint(1000000, 9999999)}"
            tag = random.choice(['tag1', 'tag2'])
            to_create.append(
                Client(phone_number=phone_number,
                       tag=tag, carrier_code='994')
            )

        Client.objects.bulk_create(to_create, batch_size=clients_to_create)
