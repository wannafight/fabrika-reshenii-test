from django.core.validators import RegexValidator, MinLengthValidator
from django.db import models
from django.db.models import QuerySet, Count
from timezone_field import TimeZoneField

from mailing.validators import validate_carrier_code, \
    validate_mailing_datetime, validate_mailing_filters


class Mailing(models.Model):
    """Model to store mailing information"""
    start_dt = models.DateTimeField(help_text="When mailing will trigger.")
    end_dt = models.DateTimeField(help_text="When mailing should stop.")
    text = models.TextField()
    carrier_code_filter = models.CharField(
        help_text="Carrier code filter", null=True,
        max_length=3, validators=[MinLengthValidator(3)]
    )
    tag_filter = models.CharField(
        max_length=10, help_text="Tag filter", null=True
    )

    def __str__(self) -> str:
        return f"Mailing({self.pk})[eta:{self.start_dt}]"

    def save(self, *args, **kwargs):
        validate_mailing_datetime(self.start_dt, self.end_dt)
        validate_mailing_filters(self.carrier_code_filter, self.tag_filter)
        super().save(*args, **kwargs)

    def count_messages_by_status(self) -> QuerySet:
        return self.messages.values('status').annotate(count=Count('status'))


class Client(models.Model):
    """Model to store information about client for mailing"""
    phone_number = models.CharField(
        max_length=11,
        unique=True,
        validators=[
            RegexValidator(
                regex=r"7\d{10}",
                message="Invalid phone number format, must be 7xxxxxxxxxx"
            )
        ]
    )
    carrier_code = models.CharField(
        max_length=3,
        validators=[MinLengthValidator(3)]
    )
    tag = models.CharField(max_length=10)
    timezone = TimeZoneField(default='UTC')

    def save(self, *args, **kwargs):
        validate_carrier_code(self.carrier_code, self.phone_number)
        super().save(*args, **kwargs)


class Message(models.Model):
    """Model to store information about mailing status"""
    SENT = 'SENT'
    PENDING = 'PENDING'
    ERROR = 'ERROR'
    STATUS_CHOICES = [
        (SENT, 'Sent'),
        (PENDING, 'Pending'),
        (ERROR, 'Error')
    ]

    sent_dt = models.DateTimeField(null=True, default=None)
    status = models.CharField(choices=STATUS_CHOICES, max_length=10,
                              default=PENDING)
    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               related_name='messages')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE,
                                related_name='messages')
