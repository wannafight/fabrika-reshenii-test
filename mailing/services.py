import logging

import requests
from django.conf import settings
from django.db.models import QuerySet
from django.utils import timezone

from mailing.models import Client, Message, Mailing


logger = logging.getLogger('app')


def filter_clients_by_mailing(mailing: Mailing) -> QuerySet[Client]:
    filters = {}
    if mailing.carrier_code_filter:
        filters['carrier_code'] = mailing.carrier_code_filter
    if mailing.tag_filter:
        filters['tag'] = mailing.tag_filter
    return Client.objects.filter(**filters)


def create_pending_messages_for_mailing(mailing: Mailing) -> None:
    logger.info(f"Creating pending messages for {mailing}")
    clients_to_notify = filter_clients_by_mailing(mailing)
    messages_to_create = [
        Message(client_id=client.pk, mailing_id=mailing.pk)
        for client in clients_to_notify
    ]
    Message.objects.bulk_create(
        messages_to_create, batch_size=len(messages_to_create)
    )
    logger.info(
        f"Created {len(messages_to_create)} "
        f"pending messages for {mailing}"
    )


def send_mailing_message_to_client(client: Client,
                                   text: str,
                                   message_id: int) -> requests.Response:
    headers = {'Authorization': f'Bearer {settings.MAILING_SERVICE_TOKEN}'}
    data = {
        'id': message_id,
        'phone': int(client.phone_number),
        'text': text,
    }
    response = requests.post(
        f"{settings.MAILING_SERVICE_URL}/send/{message_id}",
        json=data,
        headers=headers
    )
    return response


def update_message_status_based_on_response(response: requests.Response,
                                            message_id: int):
    message = Message.objects.get(pk=message_id)
    if response.ok:
        message.status = Message.SENT
        message.sent_dt = timezone.now()
    else:
        message.status = Message.ERROR
    message.save()


def send_message_and_update_status(client_id: int, message_id: int, text: str):
    client = Client.objects.filter(pk=client_id).first()
    if not client:
        logger.warning(f"Client({client_id}) does not exist")
        return
    response = send_mailing_message_to_client(client, text, message_id)
    update_message_status_based_on_response(response, message_id)
    logger.info(f"Sent message to Client({client_id})")
