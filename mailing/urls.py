from django.urls import include, path
from rest_framework.routers import DefaultRouter

from mailing.views import ClientViewSet, MailingViewSet

default_router = DefaultRouter()
default_router.register('clients', ClientViewSet, basename='clients')
default_router.register('mailings', MailingViewSet, basename='mailings')

urlpatterns = [
    path('', include(default_router.urls))
]
