import datetime

from django.core.exceptions import ValidationError


def validate_carrier_code(code: str, phone_number: str) -> None:
    if phone_number[1:4] != code:
        raise ValidationError("Carrier code does not match with phone number")


def validate_mailing_datetime(start_dt: datetime.datetime,
                              end_dt: datetime.datetime) -> None:
    if start_dt > end_dt:
        raise ValidationError(
            "Start date can not be greater than end date"
        )


def validate_mailing_filters(carrier_code_filter: str | None,
                             tag_filter: str | None) -> None:
    if not (carrier_code_filter or tag_filter):
        raise ValidationError("One of the filters must be filled")
