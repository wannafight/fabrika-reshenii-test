from datetime import datetime

from rest_framework import serializers
from timezone_field.rest_framework import TimeZoneSerializerField

from mailing.models import Client, Mailing
from mailing.services import create_pending_messages_for_mailing
from mailing.tasks import send_mailing_messages
from mailing.utils import get_value_from_dict_or_instance
from mailing.validators import validate_mailing_datetime, \
    validate_mailing_filters, validate_carrier_code as _validate_carrier_code


class ClientSerializer(serializers.ModelSerializer):
    timezone = TimeZoneSerializerField(default='UTC')

    class Meta:
        model = Client
        fields = (
            'phone_number', 'carrier_code',
            'tag', 'timezone', 'id'
        )
        extra_kwargs = {
            'carrier_code': {'min_length': 3},
            'phone_number': {'min_length': 11}
        }

    def validate_carrier_code(self, code: str):
        """Validate carrier code based on phone number"""
        phone_number = get_value_from_dict_or_instance(
            'phone_number', self.initial_data, self.instance
        )
        _validate_carrier_code(code, phone_number)
        return code


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = (
            'start_dt', 'end_dt', 'text',
            'carrier_code_filter', 'tag_filter', 'id'
        )

    def validate_start_date(self,
                            start_dt: datetime) -> datetime:
        end_dt = get_value_from_dict_or_instance(
            'end_dt', self.initial_data, self.instance
        )
        if not isinstance(end_dt, datetime):
            end_dt = datetime.fromisoformat(end_dt.replace('Z', '+00:00'))
        validate_mailing_datetime(start_dt, end_dt)
        return start_dt

    def validate_end_dt(self, end_dt: datetime) -> datetime:
        start_dt = get_value_from_dict_or_instance(
            'start_dt', self.initial_data, self.instance
        )
        if not isinstance(start_dt, datetime):
            start_dt = datetime.fromisoformat(start_dt.replace('Z', '+00:00'))
        validate_mailing_datetime(start_dt, end_dt)
        return end_dt

    def validate_tag_filter(self, tag: str | None) -> str | None:
        carrier_code_filter = get_value_from_dict_or_instance(
            'carrier_code_filter', self.initial_data, self.instance
        )
        validate_mailing_filters(carrier_code_filter, tag)
        return tag

    def validate_carrier_code_filter(self, code: str | None) -> str | None:
        tag_filter = get_value_from_dict_or_instance(
            'tag_filter', self.initial_data, self.instance
        )
        validate_mailing_filters(code, tag_filter)
        return code

    def create(self, validated_data: dict) -> Mailing:
        instance: Mailing = super().create(validated_data)
        create_pending_messages_for_mailing(instance)
        send_mailing_messages.apply_async(
            (instance.pk,), eta=instance.start_dt
        )
        return instance

    def update(self, instance: Mailing, validated_data: dict) -> Mailing:
        if 'start_dt' in validated_data \
                and self.instance.start_dt != validated_data['start_dt']:
            send_mailing_messages.apply_async(
                (instance.pk,), eta=validated_data['start_dt']
            )
        return super().update(instance, validated_data)


class MailingMessageStatsSerializer(serializers.Serializer):  # noqa
    status = serializers.CharField()
    count = serializers.IntegerField()


class MailingStatusSerializer(MailingSerializer):
    messages_stats = MailingMessageStatsSerializer(
        read_only=True, many=True, source='count_messages_by_status'
    )

    class Meta(MailingSerializer.Meta):
        fields = MailingSerializer.Meta.fields + ('messages_stats',)
