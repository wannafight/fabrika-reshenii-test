# Фабрика решений. Тестовое задание

## Стек
Python 3.10, Django 4.1, DRF, Celery, PostgreSQL, Docker, Redis

## Запуск
Через docker-compose
```shell
docker-compose up
```
Джанго без докера
```shell
docker-compose up --scale app=0

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
./manage.py runserver 0.0.0.0:8000

pyhton3 -m celery -A fabriqueStudioTest worker -l info
```

## API
[Сваггер](http://0.0.0.0:8000/docs/)
1. `/api/clients/` - эндпоинт для операций с клиентами (`GET`, `POST`, `PUT/PATCH`, `DELETE`)
2. `/api/mailings/` - эндпоинт для операций с расылками (`GET`, `POST`, `PUT/PATCH`, `DELETE`)
3. `/api/mailings/{id}/stats/` - статистика по рассылке в виде

```json
{
  "start_dt": "2022-08-09T15:06:36.715000Z",
  "end_dt": "2022-08-12T15:06:36.715000Z",
  "text": "string",
  "carrier_code_filter": "904",
  "tag_filter": "tag2",
  "id": 34,
  "messages_stats": [
    {
      "status": "SENT",
      "count": 3
    }
  ]
}
```

## Доп пункты
- подготовить docker-compose для запуска всех сервисов проекта одной командой
- сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
- логирование (часть для селери и джанго)